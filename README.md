TRACKING TOP 10 NETFLIX SERIES 

Netflix is an application that keeps growing bigger and faster with its popularity, shows, and content. It is our dearest pastime that never fails to keep up with all our favorite series and shows. 
Questions
Have you ever wondered whether your favorite series is also everyone else’s favorite? 
How well does the show perform? 
How were the ratings and their trend over time?
Have we watched every season of a series, or did we miss some?

Intention
The purpose of this project is to prepare a narrative based on our favorite Netflix series, and to answer some questions that we have posed above, and on the next page

Dataset 
We have obtained our dataset netflix.csv from - https://www.kaggle.com/datasets/timmofeyy/current-netflix-projects 
Additional data sources from:
1.	https://www.imdb.com/
2.	https://www.wikipedia.org/

Steps performed:
Web scraping and Data Scrubbing
1.	Obtained our base dataset that contains Netflix series data from Kaggle
a.	Selected top 10 from it to analyze and visualize using python.
2.	Performed data scraping from the web:
a.	From IMDB: Using the corresponding IMDB page of the series, procured data from the web page, and prepared a dataset with all the available details.
b.	From Wikipedia: Using the corresponding wiki page of the series to retrieve information about it and prepared a dataset with the details.
3.	Removed the columns that are not needed for the visualizations and scrubbed the datasets to achieve the required format.
4.	Merging Datasets: As we obtained two datasets each from IMDB and Wiki, merged them to form one single dataset for each series which was used to visualize the data.

Visualizations
1.	How many Seasons have been completed?
Using a seaborn pie chart, we have represented the percentage completed/not completed based on the ‘IsSeasonCompleted?’ column of the dataset and using ‘groupby’ to group season numbers of the series.

2.	How many episodes are there per season?
Using groupby and list functions, derive the number of episodes per season and visualized it using a seaborn barplot.

3.	What is the average rating per season?
Using the mean function to calculate the average rating per season by grouping the season numbers and representing it in a bar graph using matplotlib pyplot.

4.	How are the ratings across the seasons?
Using seaborn lineplot, plotted a graph between EpisodeNumber and Rating columns of the dataset by Season

5.	What are the top 5 episodes with the highest ratings?
Sorting the ratings of each episode in descending order and visualizing the top 5 values using a seaborn barplot

6.	What are the top 5 episodes with the lowest ratings?
Sorting the ratings of each episode in ascending order and visualizing the top 5 values using a seaborn barplot

7.	How many Episodes were directed by each Director?
Using the groupby function to group all directors and visualizing the count value using a plotly express bar graph

8.	Who are the Highest-rated Directors based on their episode rating and who directed more episodes?
By grouping the Director column values and aggregating the mean of Ratings and count of Seasons, finding if the director has directed more episodes, and sorting the values. Seaborn barplot is used here to visualize the data obtained.
